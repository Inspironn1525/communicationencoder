#define blueLed 11
#define redLed 5
#define amberLed 6
#define yellowLed 9
#define greenLed 10
#define potMeter A0
#define irTransmitter A1
#define irReceiver 2
char ace_a[] = "+";
char ace_b[] = "!!+";
char ace_c[] = "!+&";
char ace_d[] = "!+";
char ace_e[] = "!";
char ace_f[] = "!+!";
char ace_g[] = "++";
char ace_h[] = "!++";
char ace_i[] = "!!";
char ace_j[] = "+!!";
char ace_k[] = "!&";
char ace_l[] = "!!&";
char ace_m[] = "+!";
char ace_n[] = "+&";
char ace_o[] = "&+";
char ace_p[] = "&!";
char ace_q[] = "+++";
char ace_r[] = "!&+";
char ace_s[] = "++!";
char ace_t[] = "&";
char ace_u[] = "&&";
char ace_v[] = "!&!";
char ace_w[] = "+!&";
char ace_x[] = "+&!";
char ace_y[] = "!&&";
char ace_z[] = "++&";

char ace_1[] = "&!!+";
char ace_2[] = "&!!&";
char ace_3[] = "&!+!";
char ace_4[] = "&!++";
char ace_5[] = "&!+&";
char ace_6[] = "&!&!";
char ace_7[] = "&!&+";
char ace_8[] = "&!&&";
char ace_9[] = "&+!!";
char ace_0[] = "&!!!";

char ace_full_stop[] = "&!!";
char ace_comma[] = "&!+";
char ace_apostrophe[] = "&+!";
char ace_question_mark[] = "&++";
char ace_exclamation_mark[] = "&+&";
char ace_plus[] = "&&+";
char ace_minus[] = "&&!";
char ace_multiply[] = "&&&";
char ace_divide[] = "&!&";
char ace_equals[] = "&+++&";
char ace_space[] = " ";
// here the error representation is (#) symbol
char ace_error[] = "#";
char ace_wordspace[] = "_";

void setup() {
  //here are the pin mode declaration
  Serial.begin(9600);
  Serial.setTimeout(50); // for Test Harness
  pinMode(potMeter, INPUT);
  pinMode(blueLed, OUTPUT);
  pinMode(redLed, OUTPUT);
  pinMode(yellowLed, OUTPUT);
  pinMode(amberLed, OUTPUT);
  pinMode(greenLed, OUTPUT);
  pinMode(irTransmitter, OUTPUT);
  pinMode(irReceiver, INPUT);
  //These commented functions below were used to test how it works.
  //PV (yellowLed);
  //IRTX(irTransmitter);
  //IRRX(irReceiver);
  //testIRTXandIRRX();
  //FV ("fv1024");
}
//char2ace function translates the ascii character to ace symbols.
//The tolower(letter) function below change higher case letters to lower one
char *char2ace(char letter) {

  switch (tolower(letter)) {
    case 'a':
      return ace_a;
      break;
    case 'b':
      return ace_b;
      break;
    case 'c':
      return ace_c;
      break;
    case 'd':
      return ace_d;
      break;
    case 'e':
      return ace_e;
      break;
    case 'f':
      return ace_f;
      break;
    case 'g':
      return ace_g;
      break;
    case 'h':
      return ace_h;
      break;
    case 'i':
      return ace_i;
      break;
    case 'j':
      return ace_j;
      break;
    case 'k':
      return ace_k;
      break;
    case 'l':
      return ace_l;
      break;
    case 'm':
      return ace_m;
      break;
    case 'n':
      return ace_n;
      break;
    case 'o':
      return ace_o;
      break;
    case 'p':
      return ace_p;
      break;
    case 'q':
      return ace_q;
      break;
    case 'r':
      return ace_r;
      break;
    case 's':
      return ace_s;
      break;
    case 't':
      return ace_t;
      break;
    case 'u':
      return ace_u;
      break;
    case 'v':
      return ace_v;
      break;
    case 'w':
      return ace_w;
      break;
    case 'x':
      return ace_x;
      break;
    case 'y':
      return ace_y;
      break;
    case 'z':
      return ace_z;
      break;
    case '1':
      return ace_1;
      break;
    case '2':
      return ace_2;
      break;
    case '3':
      return ace_3;
      break;
    case '4':
      return ace_4;
      break;
    case '5':
      return ace_5;
      break;
    case '6':
      return ace_6;
      break;
    case '7':
      return ace_7;
      break;
    case '8':
      return ace_8;
      break;
    case '9':
      return ace_9;
      break;
    case '0':
      return ace_0;
      break;
    case '.':
      return ace_full_stop;
      break;
    case ',':
      return ace_comma;
      break;
    case '\'':
      return ace_apostrophe;
      break;
    case '?':
      return ace_question_mark;
      break;
    case '!':
      return ace_exclamation_mark;
      break;
    case '+':
      return ace_plus;
      break;
    case '-':
      return ace_minus;
      break;
    case '*':
      return ace_multiply;
      break;
    case '/':
      return ace_divide;
      break;
    case '=':
      return ace_equals;
      break;
    case ' ':
      return ace_space;
      break;
    case '_':
      return ace_wordspace;
      break;
    default:
      return ace_error;

  }
}
// ascii2ace function returns whole phrase in ace(not the single character) previouslly translated by char2ace function
String ascii2ace (String s) {

  String messege;
  for (int i = 0; i < s.length(); i++) {
    messege += char2ace(s[i]);

    if (i == s.length() - 1) {
      //  messege += '_';
    }
    else if (s[i] != *ace_space && s[i + 1] != *ace_space) {//I had problem here with pointers XDXD but the displayed error was a good hint.
      messege += ace_wordspace;
    }

  }
  return messege;
}
//ace2char function translates the ace characters to ascii symbol(It is reversed situation to char2ace function)
//In order to compare to value type 'String' the equals() method has been needed
char ace2char(String ace) {
  if (ace.equals(ace_a)) {
    return 'A';
  }
  else if (ace.equals(ace_b)) {
    return 'B';
  }
  else if (ace.equals(ace_c)) {
    return 'C';
  }
  else if (ace.equals(ace_d)) {
    return 'D';
  }
  else if (ace.equals(ace_e)) {
    return 'E';
  }
  else if (ace.equals(ace_f)) {
    return 'F';
  }
  else if (ace.equals(ace_g)) {
    return 'G';
  }
  else if (ace.equals(ace_h)) {
    return 'H';
  }
  else if (ace.equals(ace_i)) {
    return 'I';
  }
  else if (ace.equals(ace_j)) {
    return 'J';
  }
  else if (ace.equals(ace_k)) {
    return 'K';
  }
  else if (ace.equals(ace_l)) {
    return 'L';
  }
  else if (ace.equals(ace_m)) {
    return 'M';
  }
  else if (ace.equals(ace_n)) {
    return 'N';
  }
  else if (ace.equals(ace_o)) {
    return 'O';
  }
  else if (ace.equals(ace_p)) {
    return 'P';
  }
  else if (ace.equals(ace_q)) {
    return 'Q';
  }
  else if (ace.equals(ace_r)) {
    return 'R';
  }
  else if (ace.equals(ace_s)) {
    return 'S';
  }
  else if (ace.equals(ace_t)) {
    return 'T';
  }
  else if (ace.equals(ace_u)) {
    return 'U';
  }
  else if (ace.equals(ace_v)) {
    return 'V';
  }
  else if (ace.equals(ace_w)) {
    return 'W';
  }
  else if (ace.equals(ace_x)) {
    return 'X';
  }
  else if (ace.equals(ace_y)) {
    return 'Y';
  }
  else if (ace.equals(ace_z)) {
    return 'Z';
  }
  else if (ace.equals(ace_1)) {
    return '1';
  }
  else if (ace.equals(ace_2)) {
    return '2';
  }
  else if (ace.equals(ace_3)) {
    return '3';
  }
  else if (ace.equals(ace_4)) {
    return '4';
  }
  else if (ace.equals(ace_5)) {
    return '5';
  }
  else if (ace.equals(ace_6)) {
    return '6';
  }
  else if (ace.equals(ace_7)) {
    return '7';
  }
  else if (ace.equals(ace_8)) {
    return '8';
  }
  else if (ace.equals(ace_9)) {
    return '9';
  }
  else if (ace.equals(ace_0)) {
    return '0';
  }
  else if (ace.equals(ace_full_stop)) {
    return '.';
  }
  else if (ace.equals(ace_comma)) {
    return ',';
  }
  else if (ace.equals(ace_apostrophe)) {
    return '\'';
  }
  else if (ace.equals(ace_question_mark)) {
    return '?';
  }
  else if (ace.equals(ace_exclamation_mark)) {
    return '!';
  }
  else if (ace.equals(ace_plus)) {
    return '+';
  }
  else if (ace.equals(ace_minus)) {
    return '-';
  }
  else if (ace.equals(ace_multiply)) {
    return '*';
  }
  else if (ace.equals(ace_divide)) {
    return '/';
  }
  else if (ace.equals(ace_equals)) {
    return '=';
  }
  else if (ace.equals(ace_space)) {
    return ' ';
  }
  else if (ace.equals(ace_wordspace)) {
    return '_';
  }
  else {
    return '#';
  }
}
// ace2ascii has simmilar purpose to ascii2ace since it "glue"(adds the characters to one string) the ascii characters
String ace2ascii(String ace) {
  String nextChar = "";
  String result = "";
  for ( int i = 0; i < ace.length(); i++) {
    if (ace[i] == '!' || ace[i] == '+' || ace[i] == '&') {
      nextChar += ace[i];
      //if (ace.length() == 1 || i == ace.length() - 1) {
      //result += ace2char(nextChar);
      //}
    }
    else if (ace[i] == '_') {
      result += ace2char(nextChar);
      nextChar = "";
    }
    else if (ace[i] == ' ') {
      if (nextChar != "") {
        result += ace2char(nextChar);
      }
      result += " ";
      nextChar = "";
    }
  }
  if (nextChar != "") {
    result += ace2char(nextChar);
  }
  return result;
}
// sendDigital function manipulates Led flashing
void sendDigital(int led, String aceString) {
  int unit = analogRead(potMeter);
  unit = map(unit, 0, 1023, 20, 500);//map() function changes the unit compartment from current to new one
  for (int i = 0; i < aceString.length(); i++) {

    if (aceString[i] == '!') {
      digitalWrite(led, HIGH);
      delay(unit);
      digitalWrite(led, LOW);
      delay(unit);
    }
    else if (aceString[i] == '+') {
      digitalWrite(led, HIGH);
      delay(unit * 2);
      digitalWrite(led, LOW);
      delay(unit);
    }
    else if (aceString[i] == '&') {
      digitalWrite(led, HIGH);
      delay(unit * 4);
      digitalWrite(led, LOW);
      delay(unit);
    }
    else if (aceString[i] == '_') {
      //digitalWrite(led, LOW);
      delay(unit * 2);
    }
    else if (aceString[i] == ' ') {
      // digitalWrite(led, LOW);
      delay(unit * 4);

    }
  }
}
//promptReadln function return the string previouslly written by the user in serial monitor
String promptReadln() {
  while (!Serial.available()) {};
  return Serial.readString();
}
// IRTX function swith on the irled for 1.5 second then swith it off
void IRTX(int pin) {
  unsigned int frequency = 38000;
  unsigned long duration = 1500;
  tone(pin, frequency, duration);
}
/*IRRX inputs the signal by irReceivee. If the signal is being transmitted
  the output is "LOW", if there is no signal the output will be "HIGH" */
void IRRX(int pin) {
  if (digitalRead(pin) == HIGH) {
    Serial.println("HIGH");
  }
  else if (digitalRead(pin) == LOW) {
    Serial.println("LOW");
  }
}
//These function below was used to test the transmitting and reciveing the signal by IRLED and IRreceiver
/*void testIRTXandIRRX() {
  while (true) {
    tone(irTransmitter, 38000, 1500);
    if (digitalRead(irReceiver) == HIGH) {
      Serial.println("HIGH");
    }
    else if (digitalRead(irReceiver) == LOW) {
      Serial.println("LOW");
    }
  }
  }
*/
// this one flash the LED on in the proportional brightness to value in potentionometer
void PV (int pin) {
  int value = 0;
  //while (true) {
  value = analogRead(potMeter);
  value = map(value, 0, 1023, 0, 255);
  //Serial.println(analogRead(potMeter));
  analogWrite(pin, value); //which one is best?
  //tone(pin, analogRead(potMeter));

  // }
}
void FV (String fv) {
  int first = fv[2];
  int second = fv[3];
  int third = fv[4];
  int fourth = fv[5];
  int maxValue = 0;
  for (int i = 2; i < 6; i++) {
    if (fv[i] >= 48 && fv[i] <= 57) {
      if (fv[i] > maxValue) {
        maxValue = fv[i];
      }
    }
    else {
      Serial.println("Error");
      break;
    }
  }
  while (maxValue != 48 && maxValue != 0) {
    maxValue--;
    if (first != 48) {
      digitalWrite(greenLed, HIGH);
      first--;
    }
    if (second != 48) {
      digitalWrite(amberLed, HIGH);
      second--;
    }
    if (third != 48) {
      digitalWrite(yellowLed, HIGH);
      third--;
    }
    if (fourth != 48) {
      digitalWrite(redLed, HIGH);
      fourth--;
    }
    delay(125);
    digitalWrite(greenLed, LOW);
    digitalWrite(amberLed, LOW);
    digitalWrite(yellowLed, LOW);
    digitalWrite(redLed, LOW);
    delay(125);
  }
}
void loop() {
  String a, f;
  a = promptReadln();
  int i = 0;
  // isSpace() function ommit the first space characters in order to 
  // recognise the first non-space characters in given message
  while (isSpace(a[i])) {
    //Serial.print(" ");
    i++;
  }
  if (a[i] == '!' || a[i] == '&' || a[i] == '+') {
    f = ace2ascii(a);
    Serial.println(f);
    sendDigital(blueLed, a);
    if (f == "PV") {
      PV(yellowLed);
    }
    else if (f == "IRTX") {
      IRTX(irTransmitter);
    }
    else if (f == "IRRX") {
      IRRX(irReceiver);
    }
    else if (f[0] == 'F' && f[1] == 'V' && f.length() == 6) {
      FV(f);
    }
  }
  else if (a[i] != '!' || a[i] != '&' || a[i] != '+') {
    f = ascii2ace(a);
    Serial.println(f);
    sendDigital(redLed, f);
  }
}
